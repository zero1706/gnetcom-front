import { Component, OnInit } from '@angular/core';
import { InfoPaginaService } from '../../services/info-pagina.service';
import { InfoPagina } from '../../interface/info-pagina.interface';

@Component({
  selector: 'app-directiva',
  templateUrl: './directiva.component.html',
  styleUrls: ['./directiva.component.css']
})
export class DirectivaComponent implements OnInit {

  constructor(public InfoPaginaService:InfoPaginaService) { }
  infos:InfoPagina[]=[];
  ngOnInit(): void {
    this.InfoPaginaService.getUsuarios()
      .subscribe(resp=>{
      console.log(resp);
      this.infos=resp;
    })
  }

}
