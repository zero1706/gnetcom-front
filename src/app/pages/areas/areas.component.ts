import { Component, OnInit } from '@angular/core';
import { InfoAreas } from '../../interface/info-areas.interface';
import { InfoPaginaService } from '../../services/info-pagina.service';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.css']
})
export class AreasComponent implements OnInit {

  constructor(public services:InfoPaginaService) { }

  areas:InfoAreas[]=[];

  ngOnInit(): void {
    this.services.getAreas()
      .subscribe(resp=>{
      console.log(resp);
      this.areas=resp;
    })
  }

}
