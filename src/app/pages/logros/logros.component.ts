import { Component, OnInit } from '@angular/core';
import { InfoPaginaService } from '../../services/info-pagina.service';
import { Clientes } from '../../interface/info-pagina.interface';

@Component({
  selector: 'app-logros',
  templateUrl: './logros.component.html',
  styleUrls: ['./logros.component.css']
})
export class LogrosComponent implements OnInit {

  constructor(public services:InfoPaginaService) { }

  clientes:Clientes[]=[];

  ngOnInit(): void {
    this.services.getClientes()
      .subscribe(resp=>{
      console.log(resp);
      this.clientes=resp;
    })
  }

}
