import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { InfoPagina, Clientes } from '../interface/info-pagina.interface';
import { Observable } from 'rxjs';
import { InfoAreas } from '../interface/info-areas.interface';
@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {


  constructor(private http: HttpClient) { 
  
  }

  getUsuarios():Observable<InfoPagina[]>{
    return this.http.get<InfoPagina[]>('https://localhost:7247/api/Usuario/listUsuarios');  
  }
  getAreas():Observable<InfoAreas[]>{
    return this.http.get<InfoAreas[]>(' https://localhost:7247/api/Usuario/listAreas');  
  }

  getClientes():Observable<Clientes[]>{
    return this.http.get<Clientes[]>(' https://localhost:7247/api/Usuario/listClientes');  
  }

 
}
