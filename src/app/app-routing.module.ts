import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { NosotrosComponent } from './pages/nosotros/nosotros.component';
import { DirectivaComponent } from './pages/directiva/directiva.component';
import { AreasComponent } from './pages/areas/areas.component';
import { LogrosComponent } from './pages/logros/logros.component';
import { ContactoComponent } from './pages/contacto/contacto.component';

const routes: Routes = [
  {path:'inicio', component:InicioComponent},
  {path:'nosotros', component:NosotrosComponent},
  {path:'directiva', component:DirectivaComponent},
  {path:'areas', component:AreasComponent},
  {path:'logros', component:LogrosComponent},
  {path:'contacto', component:ContactoComponent},
  {path:'**', pathMatch:'full',redirectTo:'inicio'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
