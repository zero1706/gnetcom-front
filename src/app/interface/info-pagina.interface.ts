export interface InfoPagina {
    id?:          number;
    nombre?:      string;
    apellido?:    string;
    descripcion?: string;
    imagen?:      string;
}

export interface Clientes {
    id:     number;
    nombre: string;
    image:  string;
}
