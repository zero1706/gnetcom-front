export interface InfoAreas {
    id?:     number;
    nombre?: string;
    image?:  null | string;
}
